#!/usr/bin/env python3
import os
import os.path as p
import sys

import yaml

HARDCODED_SCORES_PATH = "/etc/podman-scores/scores/"

PODMAN_TARGET = """\
[Unit]
Description=Podman pods in {}

[Install]
WantedBy=multi-user.target
"""

SERVICE_TEMPLATE = """\
[Unit]
Description={} container via Podman
SourcePath={}
After=network.target
Requires=network.target

[Service]
User={}
Group={}
ExecStartPre={}
ExecStart={}
ExecStop={}
ExecStopPost={}
"""

USER = "root"
GROUP = "root"

EXEC_START_PRE = "/usr/bin/podman pull {}"
EXEC_START = "/usr/bin/podman run {}"
EXEC_STOP = "/usr/bin/podman stop {}"
EXEC_STOP_POST = "/usr/bin/podman rm {}"


def get_scores(scores_path) -> list:
    scores = []
    for f_name in os.listdir(scores_path):
        basename = p.splitext(f_name)[0]
        f = p.abspath(p.join(scores_path, f_name))
        scores.append((basename, f))
    return scores


def main(argv: list) -> int:
    if len(argv) < 4:
        return 0

    normal_dir = argv[1]
    # early_dir = argv[2]
    # late_dir = argv[3]

    for pod_name, path in get_scores(HARDCODED_SCORES_PATH):

        # load score.yaml
        with open(path) as config_file:  # TODO: handle errors
            config = yaml.safe_load(config_file)  # TODO: handle errors

        pod_target_name = f"podman-{pod_name}.target"
        pod_target_path = p.abspath(p.join(normal_dir, pod_target_name))

        services = config.get("services", {})

        container_services = []

        for container_name in services:
            properties = services[container_name]

            service_name = f"podman-{pod_name}-{container_name}.service"

            container_services.append(service_name)

            service_path = p.abspath(p.join(normal_dir, service_name))

            image = properties["image"]
            ports = properties.get("ports", [])

            start_args = []
            start_args += ["--name", container_name]

            for port in ports:
                start_args += ["-p", port]

            start_args.append(image)

            start_pre = EXEC_START_PRE.format(image)
            start = EXEC_START.format(" ".join(start_args))
            stop = EXEC_STOP.format(container_name)
            stop_post = EXEC_STOP_POST.format(container_name)

            unit = SERVICE_TEMPLATE.format(container_name, path,
                                           USER, GROUP,
                                           start_pre, start, stop, stop_post)
            with open(service_path, "w") as f:
                f.write(unit)

        with open(pod_target_path, "w") as f:
            f.write(PODMAN_TARGET.format(pod_name, ", ".join(container_services)))



if __name__ == '__main__':
    if "CONFIG_DIR" in os.environ:
        HARDCODED_SCORES_PATH = os.environ["CONFIG_DIR"]  # TODO: improve this
    sys.exit(main(sys.argv))
